import setuptools
from Cython.Build import cythonize

with open("README.md", "r") as fh:
    long_description = fh.read()

# https://pythonhosted.org/an_example_pypi_project/setuptools.html recommends, instead of the previous lines:
#def read(fname):
#    return open(os.path.join(os.path.dirname(__file__), fname)).read()
# and then:
#long_description=read('README.md'),
# below.

setuptools.setup(
    name="susceptibilities", # Replace with your own username
    version="0.0.1",
    author="Alberto Castro",
    author_email="alberto.castro.barrigon@gmail.com",
    description="Calculation of susceptibilities, using the qutip package.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/acbarrigon/susceptibilities",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: GNU GENERAL PUBLIC LICENSE",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    scripts=['susceptibilities/responsefunctions.py'],
    ext_modules = cythonize("susceptibilities/exactrfc.pyx", language_level = 3)
)

