"""exactrf module:

(...)

"""

import numpy as np

def tointeraction(H0, X, t):
    return (1j*t*H0).expm() * X * (-1j*t*H0).expm()


def rf1t(H0, V, A, state, t):
    AI = tointeraction(H0, A, t)
    if state.type == 'ket':
        y = (state.dag() * AI * V * state).full()[0, 0]
        return -1j * (y - y.conj())
    else:
        Y = AI * commutator(V, state)
        return -1j * Y.tr()


def rf2t(H0, V, A, state, t1, t2):
    h0 = H0.full()
    v = V.full()
    a = A.full()
    dim = a.shape[0]
    if state.type == 'ket':
        chi21rt = complex(0.0, 0.0)
        chi22rt = complex(0.0, 0.0)
        for k in range(dim):
            for l in range(dim):
                omegal = h0[l, l] - h0[0, 0]
                omegak = h0[k, k] - h0[0, 0]
                omegalk = h0[l, l] - h0[k, k]
                chi21rt = chi21rt - a[0, k] * v[k, l] * v[l, 0] * np.exp(-1j * omegal * t1) * np.exp(-1j * omegak * t2)
                chi22rt = chi22rt + v[0, k] * a[k, l] * v[l, 0] * np.exp(1j * omegak * t1) * np.exp(-1j * omegalk * t2)
        return 2.0 * chi21rt.real + 2.0 * chi22rt.real
    else:
        AI = tointeraction(H0, A, t1+t2)
        VI = tointeraction(H0, V, t1)
        Y = commutator(V, state)
        Y = AI * commutator(VI, Y)
        return - Y.tr()


def rf1w(H0, V, A, eta, w):
    chiw = 0j
    h0 = H0.full()
    v = V.full()
    a = A.full()
    dim = a.shape[0]
    for k in range(dim):
        chiw = chiw + (v[k, 0]*a[0, k])/(-h0[k, k]+h0[0, 0]-w+1j*eta) \
                    - (v[0, k]*a[k, 0])/(h0[k, k]-h0[0, 0]-w+1j*eta)
    return chiw / (np.sqrt(2.0*np.pi))


def rf2w(H0, V, A, eta, w1, w2):
    chi2w = complex(0.0, 0.0)
    h0 = H0.full()
    v = V.full()
    a = A.full()    
    dim = h0.shape[0]

    def chi21(omega1, omega2):
        chi21_ = complex(0.0, 0.0)
        for k in range(dim):
            omegak = h0[k, k] - h0[0, 0]
            for l in range(dim):
                omegal = h0[l, l] - h0[0, 0]
                chi21_ = chi21_ + a[0, k]*v[k, l]*v[l, 0] / ((-omegal - omega1 + 1j*eta) * (-omegak - omega2 + 1j*eta))
        return chi21_ / (2.0 * np.pi)

    def chi22(omega1, omega2):
        chi22_ = complex(0.0, 0.0)
        for k in range(dim):
            omegak = h0[k, k] - h0[0, 0]
            for l in range(dim):
                omegal = h0[l, l] - h0[0, 0]
                omegalk = h0[l, l] - h0[k, k]
                chi22_ = chi22_ + v[0, k]*a[k, l]*v[l, 0] / ((omegak - omega1 + 1j*eta) * (-omegalk - omega2 + 1j*eta))
        return -chi22_ / (2.0*np.pi)

    chi2w = chi21(w1, w2) + chi22(w1, w2) + np.conj(chi21(-w1, -w2)) + np.conj(chi22(-w1, -w2))
    return chi2w


def rf2tw(H0, V, A, eta, t1, w2):
    chi2w = complex(0.0, 0.0)
    h0 = H0.full()
    v = V.full()
    a = A.full()    
    dim = h0.shape[0]

    def chi21(t1, omega2):
        chi21_ = complex(0.0, 0.0)
        for k in range(dim):
            omegak = h0[k, k] - h0[0, 0]
            for l in range(dim):
                omegal = h0[l, l] - h0[0, 0]
                chi21_ = chi21_ + 1j * a[0, k]*v[k, l]*v[l, 0] * np.exp(-1j * omegal * t1) / (-omegak - omega2 + 1j*eta)
        return - chi21_ / np.sqrt(2.0 * np.pi)

    def chi22(t1, omega2):
        chi22_ = complex(0.0, 0.0)
        for k in range(dim):
            omegak = h0[k, k] - h0[0, 0]
            for l in range(dim):
                omegal = h0[l, l] - h0[0, 0]
                omegalk = h0[l, l] - h0[k, k]
                chi22_ = chi22_ + 1j * v[0, k]*a[k, l]*v[l, 0] * np.exp(1j * omegak * t1) / (-omegalk - omega2 + 1j*eta)
        return chi22_ / np.sqrt(2.0*np.pi)

    chi2w = chi21(t1, w2) + chi22(t1, w2) + np.conj(chi21(t1, -w2)) + np.conj(chi22(t1, -w2))
    return chi2w
