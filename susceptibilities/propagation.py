import numpy as np
from qutip import *


def propagate(H0, V, A, psi0, kappa, m, ts):
    n = ts.shape[0]
    Aav = np.zeros(n)
    psi = psi0.copy()
    while m and m[0] == 0:
        psi = (-1j * kappa * V).expm() * psi
        m.pop(0)

    j = 0

    while m:
        res = sesolve(H0, psi, ts[j:m[0]+1], [])
        psi = res.states[-1]
        for k in range(j, m[0]+1):
            Aav[k] = expect(A, res.states[k])
        j = m[0]
        while m and m[0] == j:
            psi = (-1j * kappa * V).expm() * psi
            m.pop(0)

    res = sesolve(H0, psi, ts[j:n], [A])
    Aav[j:n] = res.expect[0]

    return Aav


def propagate2(H0, V, A, psi0, kappa, m, ts):
    n = ts.shape[0]
    Aav = np.zeros(n)
    psi = psi0.copy()
    while 0 in m:
        psi = (-1j * kappa * V).expm() * psi
        m.remove(0)
    Aav[0] = expect(A, psi)
    for j in range(1, n):
        dt = ts[j]-ts[j-1]
        psi = (-1j * dt * H0).expm() * psi
        while j in m:
            psi = (-1j * kappa * V).expm() * psi
            m.remove(j)
        Aav[j] = expect(A, psi)
    return Aav


def propagate3(H0, V, A, psi0, kappa, m, ts):
    n = ts.shape[0]
    Aav = np.zeros(n)
    psi = psi0.copy()
    while 0 in m:
        psi = (-1j * kappa * V).expm() * psi
        m.remove(0)
    res = sesolve(H0, psi, ts, [A])
    Aav = res.expect[0]
    return Aav


