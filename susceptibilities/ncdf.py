"""ncdf: reads and writes the netcdf files.

(...)

"""

import os
import re
import numpy as np
from netCDF4 import Dataset


def create_ncdfile(dirname, filename):
    ncdfilename = filename

    delta_dirs = [f for f in os.listdir(dirname) if re.match('td.general-*', f)]
    ncalcs = len(delta_dirs)

    f = open( os.path.join(dirname, delta_dirs[0], "multipoles"), "r")
    lines = f.readlines()
    n = 0
    for line in lines:
        if not line.startswith("#"):
            n = n + 1

    ts = np.zeros(n)
    n = 0
    for line in lines:
        if not line.startswith("#"):
            ts[n] = line.split()[1]
            n = n + 1
    f.close()

    kappa = np.zeros(ncalcs)
    j = 0
    for i in delta_dirs:
        f = open(os.path.join(dirname, i, "multipoles"), "r")
        reg = re.compile("kick strength")
        for line in f:
            if reg.findall(line):
                kappa[j] = line.split()[3]
                break
        j = j + 1
        f.close()

    Aav = np.zeros([n, ncalcs], dtype = float)
    j = 0
    for i in delta_dirs:
        f = open(os.path.join(dirname, i, "multipoles"), "r")
        i = 0
        for line in f:
            if not line.startswith("#"):
                Aav[i, j] = line.split()[3]
                i = i + 1
        j = j + 1
        f.close()

    ncdfile_write_1st(ncdfilename, 1, n, ts, kappa, Aav)
    return None


def create_ncdfile_2(dirname, filename):
    print('Will create a netcdf file holding info to get the second-order rf, called {}'.format(filename))

    # Find out how many "kappas" have been used (a minimum of two to get the 2nd order response)
    one_delta_dirs = [f for f in os.listdir(dirname) if re.match('td.general-*', f)]
    ncalcs = len(one_delta_dirs)
    print("Found {} kappa calculations".format(ncalcs))
    print("The calculations with one delta function are stored in directories:")
    print(one_delta_dirs)

    # Now, we find out the number of time steps in the fine grid, opening one of the multipoles files.
    f = open( os.path.join(dirname, one_delta_dirs[0], "multipoles"), "r")
    lines = f.readlines()
    n2 = 0
    for line in lines:
        if not line.startswith("#"):
            n2 = n2 + 1
    n = n2 // 2
    print("The number of time steps in the propagations, n2 = {}\n".format(n2))

    # We fill in the fime time grid into netcdf variable t2
    ts2 = np.zeros(n2)
    j = 0
    for line in lines:
        if not line.startswith("#"):
            ts2[j] = line.split()[1]
            j = j + 1
    f.close()

    # We must now find the magnitudes of the kappas, and sotre it into netcdf variable kappa
    kappa = np.zeros(ncalcs)
    j = 0
    for i in one_delta_dirs:
        f = open(os.path.join(dirname, i, "multipoles"), "r")
        reg = re.compile("kick strength")
        for line in f:
            if reg.findall(line):
                kappa[j] = line.split()[3]
                break
        j = j + 1
        f.close()
    print("The kappa values used are: {}".format(kappa))

    # We now load the propagations with only one delta.
    Aav1 = np.zeros([n2, ncalcs], dtype = float)
    j = 0
    for d in one_delta_dirs:
        f = open(os.path.join(dirname, d, "multipoles"), "r")
        i = 0
        for line in f:
            if not line.startswith("#"):
                Aav1[i, j] = line.split()[3]
                i = i + 1
        j = j + 1
        f.close()
    print("Loaded the single-delta propagations")
    for k in range(ncalcs):
        Aav1[:, k] = Aav1[:, k] - Aav1[0, k]

    # Let us find out how many points in the coarse grid
    two_delta_dirs = [f for f in os.listdir(os.path.join(dirname, '2')) if re.match('td.general-0', f)]
    ncoarse = len(two_delta_dirs)
    print("ncoarse = {}".format(ncoarse))

    # Let us now find the coarse grid times.
    t1 = np.zeros(ncoarse)
    for j in range(ncoarse):
        #f = open(os.path.join(dirname, '2', 'td.general-0-{}'.format(j), 'multipoles'), "r")
        #reg = re.compile("kick time")
        #for line in f:
        #    if reg.findall(line):
        #        t1[j] = line.split()[3]
        #f.close()
        f = open(os.path.join(dirname, '2', 'td.general-0-{}'.format(j), "kicktime"), "r")
        t1[j] = f.readline()

    # We now load the propagations with two deltas.
    Aav2 = np.zeros([ncoarse, n2, ncalcs], dtype = float)
    for k in range(ncalcs):
        for j in range(ncoarse):
            f = open(os.path.join(dirname, '2', 'td.general-{}-{}'.format(k, j), 'multipoles'), "r")
            i = 0
            for line in f:
                if not line.startswith("#"):
                    Aav2[j, i, k] = line.split()[3]
                    i = i + 1

    for k in range(ncalcs):
        for j in range(ncoarse):
            Aav2[j, :, k] = Aav2[j, :, k] - Aav2[j, 0, k]

    ncdfile_write_2nd(filename, 2, n, ncoarse, t1, ts2, kappa, Aav1, Aav2)
    return None


def ncdfile_write_2nd(filename, order, n, ncoarse, tscoarse, ts2, kappa, Aav1, Aav2):
    f = Dataset(filename, 'w')
    f.description = 'second order'
    ncalcs = kappa.shape[0]
    nc_ncalcs = f.createDimension("ncalcs", ncalcs)
    nc_n = f.createDimension("n", n)
    nc_n2 = f.createDimension("n2", 2*n)
    nc_ncoarse = f.createDimension("ncoarse", ncoarse)
    nc_t1 = f.createVariable("t1", "f8", ("ncoarse"))
    nc_t2 = f.createVariable("t2", "f8", ("n2"))
    nc_Aav1 = f.createVariable("Aav1", "f8", ("n2", "ncalcs"))
    nc_Aav2 = f.createVariable("Aav2", "f8", ("ncoarse", "n2", "ncalcs"))
    nc_kappa = f.createVariable("kappa", "f8", ("ncalcs"))
    nc_t1[:] = tscoarse[:]
    nc_t2[:] = ts2[:]
    nc_Aav1[:, :] = Aav1[:, :]
    nc_Aav2[:, :, :] = Aav2[:, :]
    nc_kappa[:] = kappa[:]
    f.close()
    return None


def ncdfile_write_1st(filename, order, n, ts, kappa, Aav):
    f = Dataset(filename, 'w')
    f.description = 'first order'
    ncalcs = kappa.shape[0]
    nc_ncalcs = f.createDimension("ncalcs", ncalcs)
    nc_n = f.createDimension("time", n)
    nc_time = f.createVariable("time", "f8", ("time"))
    nc_Aav = f.createVariable("Aav", "f8", ("time", "ncalcs"))
    nc_kappa = f.createVariable("kappa", "f8", ("ncalcs"))
    nc_kappa[:] = kappa[:]
    nc_Aav[:, :] = Aav[:, :]
    nc_time[:] = ts[:]
    f.close()
    return None


def ncdfile_read(filename):
    f = Dataset(filename, 'r')
    if f.description == 'second order':
        ncalcs = len(f.dimensions["ncalcs"])
        n = len(f.dimensions["n"])
        n2 = len(f.dimensions["n2"])
        ncoarse = len(f.dimensions["ncoarse"])
        Aav1 = np.array(f.variables["Aav1"])
        Aav2 = np.array(f.variables["Aav2"])
        kappa = np.array(f.variables["kappa"])
        t1 = np.array(f.variables["t1"])
        t2 = np.array(f.variables["t2"])
        f.close()
        return ncoarse, n, ncalcs, t1, t2, kappa, Aav1, Aav2
    else:
        ncalcs = len(f.dimensions["ncalcs"])
        n = len(f.dimensions["time"])
        Aav = np.array(f.variables["Aav"])
        kappa = np.array(f.variables["kappa"])
        t = np.array(f.variables["time"])
        f.close()
        return n, ncalcs, t, kappa, Aav
