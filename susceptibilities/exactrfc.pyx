"""exactrfnew module:

(...)

"""

#from cython.view cimport array as cvarray
import numpy as np
from qutip import *


def tointeraction(H0, X, t):
    return (1j*t*H0).expm() * X * (-1j*t*H0).expm()


def rf1t(H0, V, A, state, t):
    AI = tointeraction(H0, A, t)
    if state.type == 'ket':
        y = (state.dag() * AI * V * state).full()[0, 0]
        return -1j * (y - y.conj())
    else:
        Y = AI * commutator(V, state)
        return -1j * Y.tr()


def rf2t(H0, V, A, state, double t1, double t2):
    cdef double complex[:, :] h0 = H0.full()
    cdef double complex[:, :] v = V.full()
    cdef double complex[:, :] a = A.full()
    cdef int dim, k, l
    cdef double complex omegal, omegak, omegalk, chi21rt, chi22rt
    dim = a.shape[0]
    if state.type == 'ket':
        chi21rt = complex(0.0, 0.0)
        chi22rt = complex(0.0, 0.0)
        for k in range(dim):
            for l in range(dim):
                omegal = h0[l, l] - h0[0, 0]
                omegak = h0[k, k] - h0[0, 0]
                omegalk = h0[l, l] - h0[k, k]
                chi21rt = chi21rt - a[0, k] * v[k, l] * v[l, 0] * np.exp(-1j * omegal * t1) * np.exp(-1j * omegak * t2)
                chi22rt = chi22rt + v[0, k] * a[k, l] * v[l, 0] * np.exp(1j * omegak * t1) * np.exp(-1j * omegalk * t2)
        return 2.0 * chi21rt.real + 2.0 * chi22rt.real
    else:
        AI = tointeraction(H0, A, t1+t2)
        VI = tointeraction(H0, V, t1)
        Y = commutator(V, state)
        Y = AI * commutator(VI, Y)
        return - Y.tr()


def rf2t_(H0, V, A, state, double[:] t1, double[:] t2):
    cdef double complex[:, :] h0 = H0.full()
    cdef double complex[:, :] v = V.full()
    cdef double complex[:, :] a = A.full()
    cdef int dim, k, l, n, ncoarse
    cdef double complex omegal, omegak, omegalk, chi21rt, chi22rt, I
    dim = a.shape[0]

    I = 1j
    ncoarse = t1.shape[0]
    n = t2.shape[0]
    chi2t = np.zeros([ncoarse, n], dtype = float)

    if state.type == 'ket':
        for i in range(ncoarse):
            for j in range(n):
                chi21rt = complex(0.0, 0.0)
                chi22rt = complex(0.0, 0.0)
                for k in range(dim):
                    omegak = h0[k, k] - h0[0, 0]
                    for l in range(dim):
                        omegal = h0[l, l] - h0[0, 0]
                        omegalk = h0[l, l] - h0[k, k]
                        chi21rt = chi21rt - a[0, k] * v[k, l] * v[l, 0] * np.exp(-I * omegal * t1[i] - I * omegak * t2[j])
                        chi22rt = chi22rt + v[0, k] * a[k, l] * v[l, 0] * np.exp(I * omegak * t1[i] - I * omegalk * t2[j])
                chi2t[i, j] = 2.0 * chi21rt.real + 2.0 * chi22rt.real
        return chi2t
    else:
        # WARNING: this should be fixed.
        #AI = tointeraction(H0, A, t1+t2)
        #VI = tointeraction(H0, V, t1)
        raise Exception("Code missing")
        Y = commutator(V, state)
        Y = AI * commutator(VI, Y)
        return - Y.tr()


def rf1w(H0, V, A, double eta, double w):
    cdef double complex chiw
    cdef int dim
    chiw = 0j

    cdef double complex[:, :] h0 = H0.full()
    cdef double complex[:, :] v = V.full()
    cdef double complex[:, :] a = A.full()

    dim = a.shape[0]

    cdef double complex ieta = 1j*eta
    for k in range(dim):
        chiw = chiw + (v[k, 0]*a[0, k])/(-h0[k, k] + h0[0, 0] - w + ieta) \
                    - (v[0, k]*a[k, 0])/(h0[k, k] - h0[0, 0] - w + ieta)
    return chiw / (np.sqrt(2.0*np.pi))


def rf2w(H0, V, A, double eta, double w1, double w2):
    cdef double complex chi2w
    cdef int dim

    chi2w = complex(0.0, 0.0)
    cdef double complex[:, :] h0 = H0.full()
    cdef double complex[:, :] v = V.full()
    cdef double complex[:, :] a = A.full()
    dim = h0.shape[0]
    cdef double complex ieta = 1j*eta

    def chi21(double omega1, double omega2):
        cdef double complex chi21_ = complex(0.0, 0.0)
        cdef int k, l
        cdef double complex omegak, omegal
        for k in range(dim):
            omegak = h0[k, k] - h0[0, 0]
            for l in range(dim):
                omegal = h0[l, l] - h0[0, 0]
                chi21_ = chi21_ + a[0, k]*v[k, l]*v[l, 0] / ((-omegal - omega1 + 1j*eta) * (-omegak - omega2 + ieta))
        return chi21_ / (2.0 * np.pi)

    def chi22(double omega1, double omega2):
        cdef double complex chi22_ = complex(0.0, 0.0)
        cdef int k, l
        cdef double complex omegak, omegal, omegalk
        for k in range(dim):
            omegak = h0[k, k] - h0[0, 0]
            for l in range(dim):
                omegal = h0[l, l] - h0[0, 0]
                omegalk = h0[l, l] - h0[k, k]
                chi22_ = chi22_ + v[0, k]*a[k, l]*v[l, 0] / ((omegak - omega1 + 1j*eta) * (-omegalk - omega2 + ieta))
        return -chi22_ / (2.0*np.pi)

    chi2w = chi21(w1, w2) + chi22(w1, w2) + np.conj(chi21(-w1, -w2)) + np.conj(chi22(-w1, -w2))
    return chi2w


def rf2w_(H0, V, A, double eta, double[:] w1, double[:] w2):
    #cdef double complex chi2w
    cdef int dim, ncoarse, n

    #chi2w = complex(0.0, 0.0)
    cdef double complex[:, :] h0 = H0.full()
    cdef double complex[:, :] v = V.full()
    cdef double complex[:, :] a = A.full()
    dim = h0.shape[0]
    cdef double complex ieta = 1j*eta

    def chi21(double omega1, double omega2):
        cdef double complex chi21_ = complex(0.0, 0.0)
        cdef int k, l
        cdef double complex omegak, omegal
        for k in range(dim):
            omegak = h0[k, k] - h0[0, 0]
            for l in range(dim):
                omegal = h0[l, l] - h0[0, 0]
                chi21_ = chi21_ + a[0, k]*v[k, l]*v[l, 0] / ((-omegal - omega1 + 1j*eta) * (-omegak - omega2 + ieta))
        return chi21_ / (2.0 * np.pi)

    def chi22(double omega1, double omega2):
        cdef double complex chi22_ = complex(0.0, 0.0)
        cdef int k, l
        cdef double complex omegak, omegal, omegalk
        for k in range(dim):
            omegak = h0[k, k] - h0[0, 0]
            for l in range(dim):
                omegal = h0[l, l] - h0[0, 0]
                omegalk = h0[l, l] - h0[k, k]
                chi22_ = chi22_ + v[0, k]*a[k, l]*v[l, 0] / ((omegak - omega1 + 1j*eta) * (-omegalk - omega2 + ieta))
        return -chi22_ / (2.0*np.pi)

    ncoarse = w1.shape[0]
    n = w2.shape[0]
    chi2w = np.zeros([ncoarse, n], dtype = complex)
    for i in range(ncoarse):
        for j in range(n):
            chi2w[i, j] = chi21(w1[i], w2[j]) + chi22(w1[i], w2[j]) + np.conj(chi21(-w1[i], -w2[j])) + np.conj(chi22(-w1[i], -w2[j]))

    return chi2w