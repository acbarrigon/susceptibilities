"""octopus module:

(...)

"""

import os
import re
import shutil
import numpy as np
import subprocess
import qutip as qt
from distutils.spawn import find_executable
from multiprocessing import Pool, cpu_count

octopus_dir = None
octopus_exe = None

def find_octopus(dir = None):
    """Sets the octopus executable

    If dir is present, tries to find it there. Otherwise, just checks if it is
    in the path.
    """
    global octopus_dir
    global octopus_exe
    if dir is None:
        octopus_exe_ = find_executable('octopus')
    else:
        octopus_exe_ = find_executable(os.path.join(dir, 'bin/octopus'))

    if octopus_exe_ is None:
         raise Exception('No octopus executable found')
    else:
         octopus_exe = octopus_exe_
         octopus_dir = os.path.dirname(os.path.dirname(octopus_exe))
         print("Will run octopus in "+octopus_exe)

    return None

def octopus_run(dir, inpfilecontent, extravars = [], restart_dir = None):
    global octopus_dir
    global octopus_exe

    _environ = os.environ.copy()

    os.environ['OCT_PARSE_ENV'] = '1'

    rundir = os.getcwd()
    if not os.path.exists(dir):
        os.mkdir(dir)

    if restart_dir is not None:
        if os.path.exists(os.path.join(dir, 'restart')):
            shutil.rmtree(os.path.join(dir, 'restart'))
        shutil.copytree(restart_dir, os.path.join(dir, 'restart'))

    for i in extravars:
        os.environ[i] = extravars[i]

    os.chdir(dir)
    inpfile = open("inp", "w")
    inpfile.write(inpfilecontent)
    inpfile.close()

    outputfile = open("out", "w")
    a = subprocess.run(octopus_exe, stdout = outputfile, stderr = subprocess.STDOUT, shell = True)
    outputfile.close()

    os.environ.clear()
    os.environ.update(_environ)
    os.chdir(rundir)
    return None

def get_linear(inpfilecontent, kappa):
    if os.path.exists('runs'):
        raise Exception("Directory 'runs' existed. Delete it first")
    extravariables = {'OCT_CalculationMode' : 'gs'}
    octopus_run("gs", inpfilecontent, extravariables)
    ncalcs = kappa.shape[0]
    for k in range(ncalcs):
        extravariables = {'OCT_CalculationMode' : 'td',
                          'OCT_TDDeltaStrength' : '{}'.format(kappa[k])}
        octopus_run("runs", 
                    inpfilecontent, 
                    extravariables, restart_dir = 'gs/restart')
        os.chdir('runs')
        a = subprocess.run(["mv", "td.general", "td.general-{}".format(k)])
        os.chdir('..')
    return None

def get_quadratic_old(inpfilecontent, kappa, tscoarse, n):
    extravars = {'OCT_CalculationMode' : 'gs'}
    octopus_run('runs', inpfilecontent, extravars = extravars)
    ncalcs = kappa.shape[0]
    ncoarse = tscoarse.shape[0]
    for k in range(ncalcs):
        extravars = {'OCT_CalculationMode' : 'td',
                     'OCT_TDDeltaStrength' : '{}'.format(kappa[k])}
        octopus_run('runs', inpfilecontent, extravars = extravars)
        a = subprocess.run(["mv", "runs/td.general", "runs/td.general-{}".format(k)])

    for k in range(ncalcs):
        octopus_run('runs/2', inpfilecontent, extravars = {'OCT_CalculationMode' : 'gs'})

        extravars = {'OCT_CalculationMode' : 'td',
                     'OCT_TDDeltaStrength' : '{}'.format(kappa[k]),
                     'OCT_TDDeltaKickTime' : '0.0',
                     'OCT_TDMaxSteps': '1'}
        octopus_run('runs/2', inpfilecontent, extravars = extravars)
        a = subprocess.run(["mv", "runs/2/td.general", "runs/2/td.general-initkick-{}".format(k)])

        shutil.copy('runs/2/output_iter/td.0000000/wf-st0001.obf', 'runs/2/restart/gs/0000000001.obf')
        for j in range(ncoarse):
            extravars = {'OCT_CalculationMode' : 'td',
                         'OCT_TDDeltaStrength' : '{}'.format(kappa[k]),
                         'OCT_TDDeltaKickTime' : '{}'.format(tscoarse[j]),
                         'OCT_TDMaxSteps': '{}'.format(2*n-1)}
            octopus_run('runs/2', inpfilecontent, extravars = extravars)
            a = subprocess.run(["mv", "runs/2/td.general", "runs/2/td.general-{}-{}".format(k,j)])

    return None

def get_quadratic(inpfilecontent, kappa, tscoarse, n):
    if not os.path.exists('runs'):
        print("Directory 'runs' did not exist")
        os.mkdir('runs')
    else:
        print("Directory 'runs' existed. Deleting")
        shutil.rmtree('runs')
        os.mkdir('runs')

    extravars = {'OCT_CalculationMode' : 'gs'}
    octopus_run('runs', inpfilecontent, extravars = extravars)
    ncalcs = kappa.shape[0]
    ncoarse = tscoarse.shape[0]
    for k in range(ncalcs):
        extravars = {'OCT_CalculationMode' : 'td',
                     'OCT_TDDeltaStrength' : '{}'.format(kappa[k])}
        octopus_run('runs', inpfilecontent, extravars = extravars)
        a = subprocess.run(["mv", "runs/td.general", "runs/td.general-{}".format(k)])

    # Now, we will get the runs with the two delta perturbations at time zero
    for k in range(ncalcs):
        octopus_run('runs/2', inpfilecontent, extravars = {'OCT_CalculationMode' : 'gs'})
        extravars = {'OCT_CalculationMode' : 'td',
                     'OCT_TDDeltaStrength' : '{}'.format(2.0*kappa[k]),
                     'OCT_TDDeltaKickTime' : '0.0',
                     'OCT_TDMaxSteps': '{}'.format(2*n-1)}
        octopus_run('runs/2', inpfilecontent, extravars = extravars)
        a = subprocess.run(["mv", "runs/2/td.general", "runs/2/td.general-{}-0".format(k)])

    # Then, the rest.
    for k in range(ncalcs):
        octopus_run('runs/2', inpfilecontent, extravars = {'OCT_CalculationMode' : 'gs'})
        for j in range(1, ncoarse):
            extravars = {'OCT_CalculationMode' : 'td',
                         'OCT_TDDeltaStrength' : '{}'.format(kappa[k]),
                         'OCT_TDDeltaKickTime' : '0.0',
                         'OCT_TDMaxSteps': '1'}
            octopus_run('runs/2', inpfilecontent, extravars = extravars)
            extravars = {'OCT_CalculationMode' : 'td',
                         'OCT_FromScratch'     : 'no',
                         'OCT_TDDeltaStrength' : '{}'.format(kappa[k]),
                         'OCT_TDDeltaKickTime' : '{}'.format(tscoarse[j]),
                         'OCT_TDMaxSteps': '{}'.format(2*n-1)}
            octopus_run('runs/2', inpfilecontent, extravars = extravars)
            a = subprocess.run(["mv", "runs/2/td.general", "runs/2/td.general-{}-{}".format(k,j)])

    return None


def runcalc(k, j, t, kappa, inpfilecontent, n):
    extravars = {'OCT_CalculationMode' : 'td',
                 'OCT_TDDeltaStrength' : '{}'.format(kappa),
                 'OCT_TDDeltaKickTime' : '0.0',
                 'OCT_TDMaxSteps': '1'}
    octopus_run('runs/2/tmp.{}'.format(j), inpfilecontent, extravars = extravars, restart_dir = 'runs/2/restart')
    extravars = {'OCT_CalculationMode' : 'td',
                 'OCT_FromScratch'     : 'no',
                 'OCT_TDDeltaStrength' : '{}'.format(kappa),
                 'OCT_TDDeltaKickTime' : '{}'.format(t),
                 'OCT_TDMaxSteps': '{}'.format(2*n-1)}
    octopus_run('runs/2/tmp.{}'.format(j), inpfilecontent, extravars = extravars)
    a = subprocess.run(["mv", "runs/2/tmp.{}/td.general".format(j), "runs/2/td.general-{}-{}".format(k,j)])
    f = open("runs/2/td.general-{}-{}/kicktime".format(k, j), "w")
    f.write("{}".format(t))
    f.close()
    shutil.rmtree('runs/2/tmp.{}'.format(j))
    return None


def get_quadratic_parallel(inpfilecontent, kappa, tscoarse, n, nprocs = None):
    if not os.path.exists('runs'):
        print("Directory 'runs' did not exist")
        os.mkdir('runs')
    else:
        print("Directory 'runs' existed. Deleting")
        shutil.rmtree('runs')
        os.mkdir('runs')

    extravars = {'OCT_CalculationMode' : 'gs'}
    octopus_run('runs', inpfilecontent, extravars = extravars)
    ncalcs = kappa.shape[0]
    ncoarse = tscoarse.shape[0]

    if nprocs is None:
        nprocs = 1

    for k in range(ncalcs):
        extravars = {'OCT_CalculationMode' : 'td',
                     'OCT_TDDeltaStrength' : '{}'.format(kappa[k]),
                     'OCT_TDMaxSteps': '{}'.format(2*n-1)}
        octopus_run('runs', inpfilecontent, extravars = extravars)
        a = subprocess.run(["mv", "runs/td.general", "runs/td.general-{}".format(k)])

    # Now, we will get the runs with the two delta perturbations at time zero
    for k in range(ncalcs):
        octopus_run('runs/2', inpfilecontent, extravars = {'OCT_CalculationMode' : 'gs'})
        extravars = {'OCT_CalculationMode' : 'td',
                     'OCT_TDDeltaStrength' : '{}'.format(2.0*kappa[k]),
                     'OCT_TDDeltaKickTime' : '0.0',
                     'OCT_TDMaxSteps': '{}'.format(2*n-1)}
        octopus_run('runs/2', inpfilecontent, extravars = extravars)
        a = subprocess.run(["mv", "runs/2/td.general", "runs/2/td.general-{}-0".format(k)])
        f = open("runs/2/td.general-{}-0/kicktime".format(k), "w")
        f.write("{}".format(0.0))
        f.close()

    # Then, the rest.
    for k in range(ncalcs):
        octopus_run('runs/2', inpfilecontent, extravars = {'OCT_CalculationMode' : 'gs'})
        pool = Pool(nprocs)
        arglist = []
        for j in range(1, ncoarse):
            arglist.append([k, j, tscoarse[j], kappa[k], inpfilecontent, n])
        sol = pool.starmap(runcalc, arglist)
        pool.close()
        pool.join()
    return None

def get_linear_cross_section(inpfilecontent, kappa):
    extravariables = {'OCT_CalculationMode' : 'gs'}
    octopus_run("gs", inpfilecontent, extravariables)
    extravariables = {'OCT_CalculationMode' : 'td',
                      'OCT_TDDeltaStrength' : '{}'.format(kappa)}
    octopus_run("cross-section-calculation",
                inpfilecontent, extravariables, restart_dir = 'gs/restart')
    os.chdir('cross-section-calculation')
    a = subprocess.run(os.path.join(octopus_dir, 'bin/oct-propagation_spectrum'))
    os.chdir('..')
    return None

def get_matrices(inpfilecontent, extra_states):
    extravariables = {'OCT_CalculationMode' : 'gs',
                      'OCT_ExtraStates' : '{}'.format(extra_states),
                      'OCT_Output' : 'matrix_elements',
                      'OCT_OutputMatrixElements' : 'ks_multipoles'}
    octopus_run("gs_get_matrices", inpfilecontent, extravariables)
    f = open("gs_get_matrices/static/info", "r")
    lines = list(f)#.readlines()
    f.close()

    k = 0
    energies = []
    for line in lines:
        if re.match('Eigenvalues', line):
            k = k + 2
            j = 1
            while lines[k] is not '\n':
                #print(lines[k])
                index = int(lines[k].split()[0])
                energies.append(float(lines[k].split()[2]))
                k = k + 1
            #print(index)
        else:
            k = k + 1

    h0 = np.diag(energies)
    a = np.loadtxt('gs_get_matrices/static/ks_me_multipoles.1')
    H0 = qt.Qobj(h0)
    V = qt.Qobj(a)
    A = qt.Qobj(a)
    return H0, V, A
