"""hamiltonians module: returns hamiltonian examples

This module contains one function (hamiltonian) that returns
qutip sample Hamiltonians

"""

import numpy as np
import qutip as qt

def hamiltonian(n):
    """Returns qutip objects containing a Hamiltonian example.

    Currently, only one Hamiltonian is returned ("1"): a four-level
    sample.

    Parameters
    __________

    n : int
        The Hamiltonian that is to be returned. Currently, just "1"
        is implemented.

    Returns
    _______

    Qobj, Qobj, Qobj
        The Hamiltonian, one perturbation operator V, and one observable
        operator A

    """
    if n == 1:
        h0 = np.diag([0.0, 1.0, 2.5, 2.75])
        H0 = qt.Qobj(h0)
        v1 = np.diag([0.1, 0.2, 0.15], k = 1)
        v2 = np.diag([0.3, 0.05], k = 2)
        v = v1 + v2 + (v1+v2).transpose().conj()
        V = qt.Qobj(v)
        a1 = np.diag([0.2, 0.1, 0.12], k = 1)
        a2 = np.diag([0.05, 0.3], k = 2)
        a = (a1 + a2) + (a1 + a2).transpose().conj()
        A = qt.Qobj(a)
        return H0, V, A
    else:
        print("Wrong Hamiltonian")
