"""responsefunctions module:

(...)

"""

import numpy as np
import sys
import argparse
import os
import re
#from qutip import *
from netCDF4 import Dataset
import susceptibilities.ncdf as ncdf


def nthorder(x, kappa, order):
    n = x.shape[0]
    ncalcs = x.shape[1]
    
    kappamat = np.zeros([ncalcs, ncalcs])
    for k in range(ncalcs):
        for l in range(ncalcs):
            kappamat[l, k] = kappa[k]**(l+1)   
    invkappamat = np.linalg.inv(kappamat)
    
    y = np.zeros(n, dtype = float)
    
    u = np.zeros(ncalcs)
    u[order-1] = 1.0
    alpha = np.matmul(invkappamat, u)
    
    for j in range(n):
        y[j] = 0.0
        for k in range(ncalcs):
            y[j] = y[j] + alpha[k] * x[j, k]

    return y


def chi1(filename, ofile = None):
    """Returns the first order susceptibility in real time.

    It takes as input the propagation data contained in a netcdf file.

    Parameters
    __________

    filename: string
        The name of the file that contains the propagatiions used to compute
        the susceptibility.

    ofile: string
        Then name of the file that, optionally, may be used to write the 
        results

    Returns
    _______

    numpy.ndarray, numpy.ndarray
        The susceptibility, and the array holding the times at which it is 
        calculated.

    """
    n, ncalcs, ts, kappa, Aav = ncdf.ncdfile_read(filename)

    chi1t = nthorder(Aav, kappa, order = 1)

    if ofile is not None:
        f = open(ofile, 'w')
        f.write("#\n")
        np.savetxt(f, np.column_stack([ts, chi1t]))
        f.close()

    return chi1t, ts


def chi1w(filename = None, chi1rt = None, ts = None, damping_factor = 0.01, ofile = None):
    """Returns the first order susceptibility in the frequency domain.

    It takes as input the propagation data contained in a netcdf file.

    Parameters
    __________

    filename: string
        The name of the file that contains the propagatiions used to compute
        the susceptibility.

    chirt: numpy.darray
        (...)

    ts: numpy.darray
        (...)

    damping_factor: float
        The value used to "damp" the signal before the Fourier transform is
        computed.

    ofile: string
        Then name of the file that, optionally, may be used to write the 
        results

    Returns
    _______

    numpy.ndarray, numpy.ndarray
        The susceptibility, and the array holding the frequencies at which it is 
        calculated.

    """

    if filename is not None:
        chi1rt, ts = chi1(filename)

    n = ts.shape[0]
    dt = ts[1]-ts[0]
    T = n*dt # Thisis equal to ts[-1] + dt
    ws = np.zeros(n)
    dw = 2.0*np.pi/T
    for j in range(n):
        ws[j] = j*dw

    eta = (-1.0/T)*np.log(damping_factor)
    chi1rtdamped = np.zeros(n)

    for j in range(n):
        chi1rtdamped[j] = chi1rt[j] * np.exp(-ts[j]*eta)

    chi1wdamped = np.fft.fft(chi1rtdamped.astype(complex)) * dt / np.sqrt(2.0 * np.pi)

    if ofile is not None:
        f = open(ofile, 'w')
        f.write('#\n')
        np.savetxt(f, np.column_stack([ws, chi1wdamped.real, chi1wdamped.imag]))
        f.close()

    return chi1wdamped, ws


def chi2(filename, ofile = None):
    """Returns the second order susceptibility in real time.

    """
    ncoarse, n, ncalcs, t1, t2, kappa, Aav1, Aav2 = ncdf.ncdfile_read(filename)

    Aav1_ = nthorder(Aav1, kappa, order = 2)
    Aav2_ = np.zeros([ncoarse, 2*n])
    for j in range(ncoarse):
        Aav2_[j, :] = nthorder(Aav2[j, :, :], kappa, order = 2)

    q = n // ncoarse # This should be an exact division
    chi2_ = np.zeros([ncoarse, n])
    for j in range(ncoarse):
        for k in range(n):
            chi2_[j, k] = Aav2_[j, q*j+k] - Aav1_[q*j+k] - Aav1_[k]

    if ofile is not None:
        f = open(ofile, 'w')
        f.write("#\n")
        for j in range(ncoarse):
            for i in range(n):
                #np.savetxt(f, np.column_stack([ts, chi2_]))
                f.write('{} {} {}\n'.format(t1[j], t2[i], chi2_[j, i]))
        f.close()

    return chi2_, t1, t2[0:n]


def chi2t1w2(chi2rt, t1, t2, damping_factor = 0.01):

    ncoarse = t1.shape[0]
    n = t2.shape[0]
    dtcoarse = t1[1]-t1[0]
    dt = t2[1]-t2[0]
    T = n*dt
    dw = 2.0*np.pi/T
    ws = np.zeros(n)
    for j in range(n):
        ws[j] = j*dw

    eta = (-1.0/T)*np.log(damping_factor)
    chi2rtdamped = np.zeros([ncoarse, n])
    for k in range(ncoarse):
        for j in range(n):
            chi2rtdamped[k, j] = chi2rt[k, j] * np.exp(-t2[j]*eta)

    chi2wdamped = np.zeros([ncoarse, n], dtype = complex)
    for k in range(ncoarse):
        chi2wdamped[k, :] = np.fft.fft(chi2rtdamped[k, :].astype(complex)) * dt / np.sqrt(2.0 * np.pi)

    return ws, chi2wdamped


def chi2w(filename = None, chi2rt = None, t1 = None, t2 = None, damping_factor = 0.01, ofile = None):
    """Returns the second order susceptibility in the frequency domain.

    (...)
    """

    if filename is not None:
        chi2rt, t1, t2 = chi2(filename)

    ncoarse = t1.shape[0]
    n = t2.shape[0]
    dtcoarse = t1[1]-t1[0]
    dt = t2[1]-t2[0]
    T = n*dt
    dw = 2.0*np.pi/T
    ws = np.zeros(n)
    for j in range(n):
        ws[j] = j*dw

    eta = (-1.0/T)*np.log(damping_factor)
    chi2rtdamped = np.zeros([ncoarse, n])
    for j in range(n):
        for k in range(ncoarse):
             chi2rtdamped[k, j] = chi2rt[k, j] * np.exp(-t1[k]*eta) * np.exp(-t2[j]*eta)

    chi2wdamped = np.fft.fft2(chi2rtdamped.astype(complex)) * dt * dtcoarse / (2.0 * np.pi)

    if ofile is not None:
        pass
        # Here we should write to file

    return ws, chi2wdamped




def main(arguments):

    parser = argparse.ArgumentParser()
    parser.add_argument("name", help = "File or directory containing the propagation data")

    parser.add_argument("-f", "--freq", help = "Output in the frequency domain", action = "store_true")
    parser.add_argument("-d", "--damping", help = "Damping factor", type = float, default = 0.01)
    args = parser.parse_args(args = arguments)

    if os.path.isdir(args.name):
        print("We have a directory")
        ncdfilename = args.name + '.ncdf'
        if os.path.isdir(os.path.join(args.name, "2")):
            print("Creating nectdf file for second order response function")
            ncdf.create_ncdfile_2(args.name, ncdfilename)
        else:
            print("Creating nectdf file for first order response function")
            ncdf.create_ncdfile(args.name, ncdfilename)
    else:
        print("We have a netcdf file")
        ncdfilename = args.name

    order = 1
    f = Dataset(ncdfilename)
    if(f.description == 'second order'):
        print("Second order")
        order = 2
    f.close()

    if args.freq:
        if order == 1:
            chi1w(ncdfilename, damping_factor = args.damping, ofile = 'chi1w')
        else:
            chi2w()
    else:
        if order == 1:
            chi1(ncdfilename, ofile = 'chi1t')
        else:
            chi2(ncdfilename, ofile = 'chi2t')


if __name__ == "__main__":
    main(sys.argv[1:])


