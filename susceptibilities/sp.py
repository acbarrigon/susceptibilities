import numpy as np

# For Fourier transform of real functions:
# If the number of points n is even, then the complex array shape is n/2 + 1 (odd number),
# and the first and last numbers are real.
# If the number of points n is odd, then the complex array shape is also n/2 + 1 (also an odd number)
# and the first (but not the last) number is real.


def complextoreal(z, n):
    y = np.zeros(n)
    if n%2 == 0:
        y[0] = z[0].real
        for j in range(1, n//2):
            y[2*j-1] = z[j].real
            y[2*j] = z[j].imag
        y[n-1] = z[n//2].real
    else:
        y[0] = z[0].real
        for j in range(1, n//2+1):
            y[2*j-1] = z[j].real
            y[2*j] = z[j].imag
    return y


def realtocomplex(y):
    n = y.shape[0]
    z = np.zeros(n//2+1, dtype = complex)
    if n%2 == 0:
        z[0] = y[0]
        for j in range(1, n//2):
            z[j] = complex(y[2*j-1], y[2*j])
        z[n//2] = y[n-1]
    else:
        z[0] = y[0]
        for j in range(1, n//2+1):
            z[j] = complex(y[2*j-1], y[2*j])
    return z

def realfft(x):
    n = x.shape[0]
    z = np.fft.rfft(x)
    y = complextoreal(z, n)
    return y

def irealfft(y):
    n = y.shape[0]
    z = realtocomplex(y)
    x = np.fft.irfft(z, n)
    return x

def irealfft2(y):
    n = y.shape[0]
    m = y.shape[1]
    x = y.copy()
    for j in range(m):
        z = realtocomplex(y[:, j])
        x[:, j] = np.fft.irfft(z, n)
    return x
