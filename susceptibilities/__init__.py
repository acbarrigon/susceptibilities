"""susceptibilities package

This package is used to compute susceptibilities, in real time
and in the frequency domain, from real-time calculations computed
with octopus (or otherwise)
"""

import os
import git
import pkg_resources  # part of setuptools

def githash():
    """Returns the hash of the susceptibilities git repository"""
    gitdir = os.path.dirname(__file__)+'/..'
    try:
        repo = git.Repo(gitdir)
    except git.exc.InvalidGitRepositoryError:
        return None
    return repo.head.object.hexsha

def about():
    version = pkg_resources.require("susceptibilities")[0].version
    print("Running susceptibilities version "+version)
    if githash() is not None:
        print(" (development version; hash {})".format(githash()))

