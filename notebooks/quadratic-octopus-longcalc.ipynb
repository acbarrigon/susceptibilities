{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calculation of quadratic susceptibilities with octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook demonstrates how to compute the second-order susceptibility by making real time propagations with octopus. It uses a very simple 1D model as example. Note that in order to have a non-zero second-order polarizability we need to have a system with no inversion symmetry."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    get_ipython\n",
    "    isnotebook = True\n",
    "except:\n",
    "    isnotebook = False\n",
    "\n",
    "import subprocess\n",
    "import os\n",
    "import shutil\n",
    "import re\n",
    "import sys\n",
    "import time\n",
    "import numpy as np\n",
    "import scipy as spy\n",
    "import matplotlib\n",
    "if not isnotebook:\n",
    "    matplotlib.use('Agg')\n",
    "import matplotlib.pyplot as plt\n",
    "import cvxpy as cvx\n",
    "import susceptibilities\n",
    "import susceptibilities.sp as sp\n",
    "import susceptibilities.octopus as octopus\n",
    "import susceptibilities.responsefunctions as rf\n",
    "import susceptibilities.exactrf as exactrf\n",
    "import susceptibilities.exactrfc as exactrfc\n",
    "import susceptibilities.hamiltonians as ham\n",
    "import susceptibilities.propagation as prop\n",
    "import susceptibilities.ncdf as ncdf\n",
    "from qutip import *"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "susceptibilities.about()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need to find the octopus executable. If it is the shell path, we just need to do `octopus.find_octopus()` with no argument. If is not not (or one wants to use a different octopus executable), specify the full path of the executable that is to be used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "octopus.find_octopus()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Time grids"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The goal is to compute the quadratic susceptibility $\\chi(t_1, t_2)$, and from it, via Fourier transformation, $\\chi(\\omega_1, \\omega_2)$. The first thing to fix, therefore, is the time grid -- or better, the time grids, as we have two time variables.\n",
    "\n",
    "First, for the second time variable, $t_2$, we consider a \"fine\" time grid formed by $n$ points, $t_2^0, t_2^1, \\dots, t_2^{n-1}$, that are in principle equispaced and start at zero: $t_2^0 = 0, t_2^1 = \\Delta t, t_2^2 = 2\\Delta t, \\dots t_2^{n-1} = \\Delta (n-1)$. Since, afterwards, we take Fourier transforms, a final \"ghost\" point $t_2^n = \\Delta n$ is assumed, and the signals are assumed to be periodic: $f(t_2^n) = f(t_2^0)$. The total grid length is therefore $T = t_2^n = n\\Delta$. The time $T$ is the one that defines the basic frequency, $\\Delta w = 2\\pi/T$. The spacing $\\Delta$ is the one that will be used by octopus as time-step for the time propagations.\n",
    "\n",
    "We then consider a coarser grid, with $n_c$ points separated by $\\Delta_c$. The total time $T$ of this second grid is equal to the total time of the first one, and in this case $T = n_c\\Delta^c$. This is the grid that we will use for the first time variable, $t_1$. The spacing $\\Delta^c$ determines how far apart we set the second delta perturbation: the whole procedure consists of making propagations in which a first perturbation is applied at time zero, and then a second one is applied at time $t_1^k = k\\Delta^c$.\n",
    "\n",
    "The coarse grid is \"contained\" in the fine grid, i.e. all points of the coarse grid are also points of the fine grid. The number of points in the fine grid, $n$, is a multiple of the number of points in the coarse grid: $n = qn_c$, where $q$ is an integer. In other words, one of every $q$ points of the fine grid is also a point of the coarse grid."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we set the total time $T$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = 800"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The total time implies a frequency discretization, that is the same for both grids:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dw = 2.0*np.pi / T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we fix the coarse grid: the number of points in the grid, and the time step, and the grid itself."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ncoarse = 800\n",
    "dtcoarse = T / ncoarse\n",
    "tscoarse = np.linspace(0.0, T, ncoarse, endpoint = False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The fine grid will contain `q` times the number of points in the coarse grid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "q = 50\n",
    "n = ncoarse * q\n",
    "dt = T / n\n",
    "ts = np.linspace(0.0, T, n, endpoint = False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#ws = np.zeros(n)\n",
    "#for j in range(n):\n",
    "#    ws[j] = dw * j"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Fourier transform is \"damped\". This means that we will be calculating:\n",
    "\\begin{equation}\n",
    "\\chi(\\omega_1, \\omega_2) = \\frac{1}{2\\pi}\\int_0^T\\!{\\rm d}t_1 e^{-\\eta t_1} e^{-i\\omega_1 t_1}\n",
    "\\int_0^T{\\rm d}t_2 e^{-\\eta t_2} e^{-i\\omega_2 t_2} \\chi(t_1, t_2)\\,.\n",
    "\\end{equation}\n",
    "We need to fix the $\\eta$ factor. We do so by fixing a damping factor, equal to $e^{-i\\eta T}$. This sets how much the signal is reduced at $t=T$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "damping_factor = 0.01\n",
    "eta = (-1.0/T)*np.log(damping_factor)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# System: inp file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we write the `inp` file for the octopus calculations. It does not need to contain all variables, as some are set at run time by the python script. For example, we do not need to set here the `CalculationMode`, the `TDMaxSteps`. the `TTDDeltaStrength`, or the `TDDeltaKickTime`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "inpfilecontent = \\\n",
    "\"\"\"\n",
    "FromScratch = yes\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "Dimensions = 1\n",
    "TheoryLevel = independent_particles\n",
    "\n",
    "Radius = 8.0\n",
    "Spacing = 0.5\n",
    "\n",
    "%Species\n",
    "\"Hydrogen1D\" | species_user_defined | potential_formula | \"-1/sqrt(1+(x)^2)\" | valence | 1\n",
    "\"Helium1D\" | species_user_defined | potential_formula | \"-2/sqrt(1+(x)^2)\" | valence | 2\n",
    "%\n",
    "\n",
    "ExcessCharge = 2\n",
    "\n",
    "%Coordinates\n",
    "\"Hydrogen1D\" | -1.5\n",
    "\"Helium1D\" | 1.5\n",
    "%\n",
    "\n",
    "TDPropagator = etrs\n",
    "TDTimeStep = {dt}\n",
    "\n",
    "TDPolarizationDirection = 1\n",
    "\n",
    "\"\"\".format(dt = dt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calculation of the second order rf in real time, with octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need to set here the values of the delta perturbation magnitudes. In order to get the quadratic response, we need a minimum of two (`ncalcs`= 2) calculations with different magnitudes. The easiest way to proceed is to use $-\\kappa$ and $-\\kappa$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kappa_ = 0.001\n",
    "ncalcs = 2\n",
    "kappa = np.array([kappa_, -kappa_])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `get_quadratic_parallel` is the that launches the octopus calculations. We need to pass the basic inp file (`inpfilecontent`), the values of the delta perturbation magnitudes (`kappa`), an array with the coarse grid (where the second delta perturbation is applied, `tscoarse`), the number of points in the fine grid (`n`). If the calculations can be performed in parallel, one may also pass a `nprocs` variable with the number of available cores."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t0 = time.time()\n",
    "octopus.get_quadratic_parallel(inpfilecontent, kappa, tscoarse, n, nprocs = 10)\n",
    "t1 = time.time()\n",
    "print(t1-t0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `get_quadratic_parallel` function stores all the generated data in a directory called `runs`. The next step is to create a netcdf file from it. It can be moved (to postprocess the data in another computer, for example), and it is way smaller."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ncdf.create_ncdfile_2('runs', 'runs.ncdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we may compute the quadratic response function. The first step is to compute it in real time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chi2t_o, dump, dump = rf.chi2('runs.ncdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot it to see how it looks (this is important to get an idea of whether $T$ is long enough, or if the time discretization is small enough)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 2, figsize = (12, 4))\n",
    "\n",
    "ax[0].plot(ts, chi2t_o[0, :], marker = '.', linewidth = 1, label = '(0, t$_2$)')\n",
    "ax[1].plot(tscoarse, chi2t_o[:, 50], marker = '.', linewidth = 1, label = '(t$_1$, 0)')\n",
    "\n",
    "ax[0].set_xlim(left = 0, right = T)\n",
    "ax[1].set_xlim(left = 0, right = 100.0)\n",
    "\n",
    "ax[0].legend()\n",
    "ax[1].legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we compute the response function in the frequency domain. We need to pass the damping factor. The variable `ws` contains the Fourier grid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ws, chi2w_o = rf.chi2w(filename = 'runs.ncdf', damping_factor = damping_factor)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us also plot it to see how it looks. We will plot both real and imaginary part."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(2, 2, figsize = (12, 8))\n",
    "\n",
    "ax[0, 0].plot(ws[:n//2], chi2w_o[0, :n//2].real, marker = '.',\n",
    "              label = 'Re $\\chi(0, \\omega_2)$')\n",
    "ax[0, 1].plot(ws[:ncoarse//2], chi2w_o[:ncoarse//2, 0].real, marker = '.',\n",
    "              label = 'Re $\\chi(\\omega_1, 0)$')\n",
    "\n",
    "ax[1, 0].plot(ws[:n//2], chi2w_o[0, :n//2].imag, marker = '.',\n",
    "              label = 'Im $\\chi(0, \\omega_2)$')\n",
    "ax[1, 1].plot(ws[:ncoarse//2], chi2w_o[:ncoarse//2, 0].imag, marker = '.',\n",
    "              label = 'Im $\\chi(\\omega_1, 0)$')\n",
    "\n",
    "ax[0, 0].legend()\n",
    "ax[0, 1].legend()\n",
    "ax[1, 0].legend()\n",
    "ax[1, 1].legend()\n",
    "\n",
    "ax[0, 0].set_xlim(left = 0.1, right = 2.0)\n",
    "ax[0, 1].set_xlim(left = 0.1, right = 2.0)\n",
    "ax[1, 0].set_xlim(left = 0.1, right = 2.0)\n",
    "ax[1, 1].set_xlim(left = 0.1, right = 2.0)\n",
    "\n",
    "ax[0, 0].set_ylim(bottom = -30, top = 30.0)\n",
    "ax[1, 0].set_ylim(bottom = -30, top = 30.0)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calculation of the dynamic hyperpolarizability with qutip: exact"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How good is the previous calculation? For this particularly simple case, we can compute the exact result."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we are going to compute the susceptibility, but using the exact sumo-over-states formula, implemented in the susceptibilities.exactrf module. In order to do that, we need to get the matrices explicitly; this can be done with the \"get_matrices\" function. The number \"32\" is the number of extra states that are added, in addition to the one state corresponding to the only one electron in the system. The reason is that the spatial grid has 33 points, and therefore the numerical Hilbert space has dimension 33. \n",
    "\n",
    "Note that this only works for one-electron problems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H0, V, A = octopus.get_matrices(inpfilecontent, 32)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will define a frequency grid (it does not need to be related to the previous frequency grid)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wsexact = np.linspace(0.0, 2.0, 200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t0 = time.time()\n",
    "chi2w_exact = exactrfc.rf2w_(H0, -V, -A, eta, wsexact, wsexact)\n",
    "t1 = time.time()\n",
    "print(t1-t0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, here we can compare the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(2, 2, figsize = (12, 8))\n",
    "\n",
    "ax[0, 0].plot(wsexact, chi2w_exact[0, :].real, marker = '.', \n",
    "              label = 'Re $\\chi(0, \\omega_2)$, exact')\n",
    "ax[0, 1].plot(wsexact, chi2w_exact[:, 0].real, marker = '.',\n",
    "              label = 'Re $\\chi(\\omega_1$, 0), exact')\n",
    "ax[0, 0].plot(ws[:n//2], chi2w_o[0, :n//2].real, marker = '.',\n",
    "              label = 'Re $\\chi(0, \\omega_2)$, octopus')\n",
    "ax[0, 1].plot(ws[:ncoarse//2], chi2w_o[:ncoarse//2, 0].real, marker = '.',\n",
    "              label = 'Re $\\chi(\\omega_1, 0)$, octopus')\n",
    "\n",
    "ax[1, 0].plot(wsexact, chi2w_exact[0, :].imag, marker = '.',\n",
    "              label = 'Im $\\chi(0, \\omega_2)$, exact')\n",
    "ax[1, 1].plot(wsexact, chi2w_exact[:, 0].imag, marker = '.',\n",
    "              label = 'Im $\\chi(\\omega_1, 0)$, exact')\n",
    "ax[1, 0].plot(ws[:n//2], chi2w_o[0, :n//2].imag, marker = '.',\n",
    "              label = 'Im $\\chi(0, \\omega_2)$, octopus')\n",
    "ax[1, 1].plot(ws[:ncoarse//2], chi2w_o[:ncoarse//2, 0].imag, marker = '.',\n",
    "              label = 'Im $\\chi(\\omega_1, 0)$, octopus')\n",
    "\n",
    "ax[0, 0].legend()\n",
    "ax[0, 1].legend()\n",
    "ax[1, 0].legend()\n",
    "ax[1, 1].legend()\n",
    "\n",
    "ax[0, 0].set_xlim(left = 0.1, right = 2.0)\n",
    "ax[0, 1].set_xlim(left = 0.1, right = 2.0)\n",
    "ax[1, 0].set_xlim(left = 0.1, right = 2.0)\n",
    "ax[1, 1].set_xlim(left = 0.1, right = 2.0)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results are not bad, although not perfect. I think that the exact match can be obtained in the limit $T \\to \\infty$ and $\\Delta \\to 0$. In other words, the only error is numerical, and the formulas are well implemented. The errors seem to appear at low frequencies (in fact, I have not plotted the lowest part of the spectrum)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
