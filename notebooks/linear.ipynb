{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calculation of linear susceptibilities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook does not use octopus, but it is used as a previous check that things work. The idea is to use a four-state model to:\n",
    "\n",
    "* Compute, using the exact formula, the first-order susceptibility in both the time and frequency domains.\n",
    "* Compute, using time propagations, the same first-order susceptibility, using a normal Fourier transform to get the frequency domain representation.\n",
    "* Get the susceptibility in the frequency domain by doing the Fourier transform from the real time signal recorded in two different time grids, a fine and a coarse grid. This shows how the use of a too coarse grid does not provide a good susceptibility in the frequency domain.\n",
    "* Finally, repeat the same computation, but using compressed sensing in order to compute the Fourier transform. This will show a better susceptibility using as input the same number of points in the time axis as the number of points of the coarse grid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    get_ipython\n",
    "    isnotebook = True\n",
    "except:\n",
    "    isnotebook = False\n",
    "\n",
    "import numpy as np\n",
    "import scipy as spy\n",
    "import matplotlib\n",
    "if not isnotebook:\n",
    "    matplotlib.use('Agg')\n",
    "import matplotlib.pyplot as plt\n",
    "import cvxpy as cvx\n",
    "import susceptibilities\n",
    "import susceptibilities.sp as sp\n",
    "import susceptibilities.responsefunctions as rf\n",
    "import susceptibilities.exactrf as exactrf\n",
    "import susceptibilities.exactrfc as exactrfc\n",
    "import susceptibilities.hamiltonians as ham\n",
    "import susceptibilities.propagation as prop\n",
    "import susceptibilities.ncdf as ncdf\n",
    "from netCDF4 import Dataset\n",
    "import qutip as qt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "susceptibilities.about()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Definition of the Hamiltonian"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `hamiltonian` returns an unperturbed Hamiltonian `H0`, pertubation operator `V`, and an observable `A`. The goal will be to compute the corresponding linear susceptibility $\\chi_{\\hat{A},\\hat{V}}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H0, V, A = ham.hamiltonian(1)\n",
    "print(H0)\n",
    "print(V)\n",
    "print(A)\n",
    "max_frequency = H0.full()[-1, -1].real"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Time grids"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We consider a time grid formed by $n$ points, $t_0, t_1, \\dots, t_{n-1}$, that are in principle equispaced and start at zero, $t_0 = 0, t_1 = \\Delta t, t_2 = 2\\Delta t, \\dots t_{n-1} = \\Delta (n-1)$. Since, afterwards, we take Fourier transforms, a final \"ghost\" point $t_n = \\Delta n$ is assumed, and the signals are assumed to be periodic: $f(t_n) = f(t_0)$. The total propagation time interval is therefore $T = t_n = n\\Delta$, although no propagation is necessary from $t_{n-1}$ to $t_n = T$. The time $T$ is the one that defines the basic frequency, $\\Delta w = 2\\pi/T$.\n",
    "\n",
    "We also consider a coarse grid, with $n_c$ points separated by $\\Delta_c$, and equal definition. The total time $T$ is equal, and in this case $T = t^c_{n_c} = n_c\\Delta^c$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = 200"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dw = 2*np.pi/T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ncoarse = 100\n",
    "m = ncoarse\n",
    "dtcoarse = T/ncoarse\n",
    "tscoarse = np.linspace(0.0, T, ncoarse, endpoint = False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "q = 10\n",
    "n = ncoarse * q\n",
    "dt = T/n\n",
    "ts = np.linspace(0.0, T, n, endpoint = False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Fourier grid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ws = np.linspace(0.0, dw*n, n, endpoint = False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Brute-force calculation of the first-order response function thought the exact formulas."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We compute, though the exact formula, the susceptibility in real time, in both the fine and the coarse grids."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chi = np.zeros(n, dtype = float)\n",
    "for j in range(n):\n",
    "    chi[j] = exactrf.rf1t(H0, V, A, qt.basis(4, 0),\n",
    "                                            t = ts[j]).real\n",
    "\n",
    "chicoarse = np.zeros(ncoarse, dtype = float)\n",
    "for j in range(ncoarse):\n",
    "    chicoarse[j] = exactrf.rf1t(H0, V, A, qt.basis(4, 0),\n",
    "                                                   t = tscoarse[j]).real"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot the response function in real time -- both the function computed in the fine and in the coarse grid. It is clear how the coarse grid seems to be insufficient."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(ts, chi, label = 'chi, fine grid')\n",
    "ax.plot(tscoarse, chicoarse, marker = '.', linewidth = 0, label = 'chi, coarse grid')\n",
    "ax.legend()\n",
    "if isnotebook:\n",
    "    plt.show()\n",
    "else:\n",
    "    fig.savefig('chirt1.pdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we compute the susceptibility in the frequency domain, using the analytical exact formula."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we need to set a $\\eta$ value, since the formula is ill-defined at resonances. In practice, one normally defines the susceptibility in the frequency domain as:\n",
    "\\begin{equation}\n",
    "\\chi_{\\hat{A},\\hat{V}}^{(1)}(\\omega) = \n",
    "\\frac{1}{\\sqrt{2\\pi}}\\sum_K \\left(\n",
    "\\frac{V_{K0}A_{0K}}{-\\omega_K-\\omega + i\\eta}\n",
    "-\n",
    "\\frac{V_{0K}A_{K0}}{\\omega_K-\\omega + i\\eta} \n",
    "\\right)\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is obtained as the Fourier transform of the first order response in real time, *but* multiplied by a damping term $e^{-\\eta t}$. Here we fix the value of $\\eta$ by first setting a \"damping factor\" $D$, which is defined as $e^{-\\eta T} = D$ (physically, it fixes by how much we reduce the value of the signal at the final time $T$)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "damping_factor = 0.01\n",
    "eta = (-1.0/T)*np.log(damping_factor)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chiw = np.zeros(n, dtype = complex)\n",
    "for j in range(n):\n",
    "    chiw[j] = exactrfc.rf1w(H0, V, A, eta, ws[j])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We plot the susceptibility in the frequency domain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(ws, chiw.real, label = 'chiw, real part')\n",
    "ax.plot(ws, chiw.imag, marker = '.', linewidth = 1, label = 'chiw, imag part')\n",
    "ax.set_xlim(left = 0.0, right = max_frequency)\n",
    "ax.legend()\n",
    "if isnotebook:\n",
    "    plt.show()\n",
    "else:\n",
    "    fig.savefig('chiw.pdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Time-propagation calculation of the first-order response function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have computed the exact susceptibilities, let us recompute them by using the delta-perturbation + propagation technique. We will use two propagations; although in principle one only needs one, the use of two propagations permits to \"clean\" the signal from the second-order noise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ncalcs = 2\n",
    "kappa = np.zeros(ncalcs)\n",
    "kappa = np.array([0.001, -0.001])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Aav = np.zeros([n, ncalcs], dtype = float)\n",
    "for k in range(ncalcs):\n",
    "    Aav[:, k] = prop.propagate(H0, V, A, qt.basis(4, 0), kappa[k], [0,], ts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have the response of the system computed in array `Aav`, we place this into a netcdf file. For that we need the `ncdfile_write_1st` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ncdf.ncdfile_write_1st('lineartest.ncdf', 1, n, ts, kappa, Aav)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we process the netcdf file with the `chi1` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chirt, dump = rf.chi1(\"lineartest.ncdf\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us compare the response function (in real time), computed in this way, with the one computed through the exact formula."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(ts, chi, marker = '.', markersize = 2)\n",
    "ax.plot(ts, chirt)\n",
    "ax.set_xlim(left = 0, right = ts[-1]+dt)\n",
    "if isnotebook:\n",
    "    plt.show()\n",
    "else:\n",
    "    fig.savefig('chirt2.pdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we want the Fourier transform, and compare it with the one computed through the explicit formula."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chiwnum, ws = rf.chi1w(chi1rt = chi, ts = ts, damping_factor = damping_factor)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(ws[0:n//2], chiwnum[0:n//2].real, marker = '.', linewidth = 0, label = 'chiw (prop), real part')\n",
    "ax.plot(ws[0:n//2], chiwnum[0:n//2].imag, marker = '.', linewidth = 0, label = 'chiw (prop), imag part')\n",
    "ax.plot(ws[0:n//2], chiw[0:n//2].real, label = 'chiw (exact), real part')\n",
    "ax.plot(ws[0:n//2], chiw[0:n//2].imag, label = 'chiw (exact), imag part')\n",
    "ax.set_xlim(left = 0.0, right = max_frequency)\n",
    "ax.legend()\n",
    "if isnotebook:\n",
    "    plt.show()\n",
    "else:\n",
    "    fig.savefig('chiw.pdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Recomputation of the susceptibility in the Fourier domain using a coarser grid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chicoarsewnum, wscoarse = rf.chi1w(chi1rt = chicoarse, ts = tscoarse,\n",
    "                                   damping_factor = damping_factor)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We plot the susceptibility computed in the coarse grid. It becomes clear how it is much worse. In fact, we have chosen the time step so large that the maximum frequency associated to it is smaller than the second peak of the response function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(ws[:ncoarse//2], chicoarsewnum[:ncoarse//2].real, marker = '.', linewidth = 0,\n",
    "        label = 'chiw (coarse), real part')\n",
    "ax.plot(ws[:ncoarse//2], chicoarsewnum[:ncoarse//2].imag, marker = '.', linewidth = 0,\n",
    "        label = 'chiw (coarse), imag part')\n",
    "ax.plot(ws[0:n//2], chiw[0:n//2].real, label = 'chiw (exact), real part')\n",
    "ax.plot(ws[0:n//2], chiw[0:n//2].imag, label = 'chiw (exact), imag part')\n",
    "ax.set_xlim(left = 0.0, right = max_frequency)\n",
    "ax.legend()\n",
    "if isnotebook:\n",
    "    plt.show()\n",
    "else:\n",
    "    fig.savefig('chiw.pdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Compressed sensing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us compute the susceptibility in the frequncy domain by making use of compressed sensing, to see if one can obtain a better result using the same number of points as the coarse grid."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First of all, one must get a random set of points from the fine grid, that will serve as a new coarse grid (it is important that this new coarse grid, although it has the same number of points as the previous coarse grid, it has them distributed randomly)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ri = np.random.choice(n, m, replace = False)\n",
    "ri.sort()\n",
    "t2 = ts[ri]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we get the susceptibiliy in real time in this new coarse random time grid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chirnd = chi[ri]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We \"damp\" the function in real time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chidampedrnd = np.copy(chirnd)\n",
    "for j in range(ncoarse):\n",
    "    chidampedrnd[j] = chirnd[j] * np.exp(-t2[j-1]*eta)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the compressed sensing procedure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = sp.irealfft2(np.identity(n))\n",
    "A = A[ri]\n",
    "#print(A.shape)\n",
    "#print(A.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v = cvx.Variable(n)\n",
    "objective = cvx.Minimize(cvx.norm(v, 1))\n",
    "constraints = [A@v == chidampedrnd]\n",
    "prob = cvx.Problem(objective, constraints)\n",
    "result = prob.solve(verbose = False)\n",
    "x = np.array(v.value)\n",
    "zx = sp.realtocomplex(x) * dt / np.sqrt(2.0*np.pi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And finally, we plot the results, to see how everything looks. First, the real part:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(ws[:n//2], zx[:n//2].real, marker = '.', linewidth = 1, label = 'chiw (CS), real part')\n",
    "#ax.plot(ws[:n//2], zx[:n//2].imag, marker = '.', linewidth = 1, label = 'chiw (CS), imag part')\n",
    "ax.plot(ws[0:n//2], chiw[0:n//2].real, label = 'chiw (exact), real part')\n",
    "#ax.plot(ws[0:n//2], chiw[0:n//2].imag, label = 'chiw (exact), imag part')\n",
    "ax.set_xlim(left = 0.0, right = max_frequency)\n",
    "ax.legend()\n",
    "if isnotebook:\n",
    "    plt.show()\n",
    "else:\n",
    "    fig.savefig('chiw.pdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, the imaginary part:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "#ax.plot(ws[:n//2], zx[:n//2].real, marker = '.', linewidth = 1, label = 'chiw (CS), real part')\n",
    "ax.plot(ws[:n//2], zx[:n//2].imag, marker = '.', linewidth = 1, label = 'chiw (CS), imag part')\n",
    "#ax.plot(ws[0:n//2], chiw[0:n//2].real, label = 'chiw (exact), real part')\n",
    "ax.plot(ws[0:n//2], chiw[0:n//2].imag, label = 'chiw (exact), imag part')\n",
    "ax.set_xlim(left = 0.0, right = max_frequency)\n",
    "ax.legend()\n",
    "if isnotebook:\n",
    "    plt.show()\n",
    "else:\n",
    "    fig.savefig('chiw.pdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Compressed sensing, sine transformation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dws = 0.5 * (2.0*np.pi/T)\n",
    "wss = np.zeros(n)\n",
    "for j in range(n):\n",
    "    wss[j] = dws * (j+1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = spy.fft.idst(np.identity(n), type =1)\n",
    "A = A[ri]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v = cvx.Variable(n)\n",
    "objective = cvx.Minimize(cvx.norm(v, 1))\n",
    "constraints = [A@v == chidampedrnd]\n",
    "prob = cvx.Problem(objective, constraints)\n",
    "result = prob.solve(verbose = False)\n",
    "x = np.array(v.value)\n",
    "x = -x * dt / np.sqrt(2.0*np.pi)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(wss[:n//2], x[:n//2], marker = '.', linewidth = 1, label = 'chiw (CS), imag part')\n",
    "ax.plot(ws[0:n//2], chiw[0:n//2].imag, label = 'chiw (exact), imag part')\n",
    "ax.set_xlim(left = 0.0, right = max_frequency)\n",
    "ax.legend()\n",
    "if isnotebook:\n",
    "    plt.show()\n",
    "else:\n",
    "    fig.savefig('chiw.pdf')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
