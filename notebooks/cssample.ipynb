{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Compressed Sensing demonstration\n",
    "\n",
    "This notebook demonstrates the use of compressed sensing to approximate the Fourier spectrum of a signal, using far less data points than the ones needed using a \"normal\" discrete Fourier transform.\n",
    "\n",
    "This notebook is based on the explanations given in:\n",
    "\n",
    "http://www.pyrunner.com/weblog/2016/05/26/compressed-sensing-python/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.optimize as spopt\n",
    "import scipy.fftpack as spfft\n",
    "import scipy.ndimage as spimg\n",
    "import cvxpy as cvx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Function definition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we define the function that we wish to process, composed of three frequency components, times a damping function.\n",
    "\n",
    "\\begin{equation}\n",
    "f(t) = (a_1\\sin(\\omega_1 t) + a_2\\sin(\\omega_2 t) + a_3\\sin(\\omega_3 t)) e^{-\\gamma t}\\,.\n",
    "\\end{equation}\n",
    "\n",
    "It is defined in the $[0,T]$ ($T = 1/8$) time interval. The $\\Delta \\omega$ frequency discretization associated to this interval is therefore $2\\pi/T = \\Delta\\omega$. We will define the frequencies $\\omega_1,\\omega_2,\\omega_3$ in relationship to this value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = 1/8\n",
    "dw = 2.0*np.pi/T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w1 = 250.2*dw\n",
    "w2 = 780.7*dw\n",
    "w3 = 552.1*dw\n",
    "a1 = 3\n",
    "a2 = 2\n",
    "a3 = 1\n",
    "gamma = 10\n",
    "print(w1, w2, w3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(t):\n",
    "    return (a1*np.sin(w1*t) + a2*np.sin(w2*t) + a3*np.sin(w3*t))*np.exp(-gamma*t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now define two time grids, a fine and a coarse one, to  hold the function values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 5000\n",
    "dt = T/n\n",
    "t = np.linspace(0, 1/8-dt, n)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ncoarse = 500\n",
    "m = ncoarse\n",
    "dtcoarse = T/ncoarse\n",
    "tcoarse = np.linspace(0, 1/8-dtcoarse, ncoarse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The corresponding discretization interval in the frequency domain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w = np.zeros(n)\n",
    "for k in range(n):\n",
    "    w[k] = dw*k"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we store in the `y` and `ycoarse` variables the values of the function in both time grids"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = f(t)\n",
    "ycoarse = f(tcoarse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function looks like this, using the two different grids (top panels: fine discretization; bottom panels: coarse discretization; on the right panels, a zoom of the first part of the interval):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(2, 2)\n",
    "ax[0, 0].plot(t, y)\n",
    "ax[0, 1].plot(t, y)\n",
    "ax[1, 0].plot(tcoarse, ycoarse)\n",
    "ax[1, 1].plot(tcoarse, ycoarse)\n",
    "ax[0, 0].set_xlim(left = 0.0)\n",
    "ax[0, 1].set_xlim(left = 0.0, right = 0.02)\n",
    "ax[1, 0].set_xlim(left = 0.0)\n",
    "ax[1, 1].set_xlim(left = 0.0, right = 0.02)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Definition of the Fourier transforms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We want to compute the Fourier spectrum of this function. We define the Fourier transform as:\n",
    "\\begin{equation}\n",
    "\\tilde{f}(\\omega) = \\frac{1}{\\sqrt{T}}\\int_0^T\\!\\!{\\rm d}t\\;f(t)e^{-i\\omega t} \\approx\n",
    "\\end{equation}\n",
    "\n",
    "Given a time grid as the ones defined above, we have an array of function values:\n",
    "\\begin{equation}\n",
    "f_j = f(x_j)\n",
    "\\end{equation}\n",
    "at $x_j = j\\Delta t$ ($j=0,\\dots,N-1$). Note that the final time of the interval $T = N\\Delta t$ is not included, as the Fourier treatment implicitly assumes periodicity, $f(0)=f(T)$.\n",
    "\n",
    "At the array of Fourier frequencies $\\omega_k = \\frac{2\\pi}{T}k$, we have:\n",
    "\\begin{equation}\n",
    "\\tilde{f}(\\omega_k) \\approx \\tilde{f}_k = \n",
    "\\frac{\\sqrt{\\Delta t}}{\\sqrt{N}}\\sum_{j=0}^{N-1} f_j e^{-i\\frac{2\\pi}{N}jk}\\,.\n",
    "\\end{equation}\n",
    "\n",
    "The set $\\tilde{f}_k$ can be computed using the FFT. For example, the one implemented in fftpack. We will use it to get the reference Fourier transform, using the fine time grid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "yw = spfft.fft(y.astype(complex))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function computes the unnormalized $\\sum_{j=0}^{N-1} f_j e^{-i\\frac{2\\pi}{N}jk}$ values. We choose to normalize these values as defined above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "yw = yw * np.sqrt(dt/n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot this spectrum. This is the one that we will use as a reference."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(w[:n//2], np.abs(yw[:n//2]))\n",
    "ax.set_xlim(left=0.0)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The maximum frequency resolved by this Fourier transform is given by $(2\\pi/\\Delta t)$: the finer the discretization, the higher frequencies one can see. We may see how our fine grid is good enough to resolve the Fouier spectrum of our signal. The cost is the use of all the data points in the fine grid."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In practice, we are computing the Fourier transform of a real function. For that purpose, it is better to use a \"real\" Fourier transform, that transforms a real array into a real array. Also, this will help to implement more easily the compressed sensing minimization, that is easier if it only handles real numbers. Therefore, we use the real Fourier transform also implemented in fftpack. If the complex coefficients obtained by the normal FFT are, as discussed above:\n",
    "\\begin{equation}\n",
    "z_k = \\sum_{j=0}^{N-1} f_j e^{-i\\frac{2\\pi}{N}jk},\n",
    "\\end{equation}\n",
    "the real FFT returns $\\tilde{y}_k$ defined in the rfft function in fftpack to be:\n",
    "\\begin{align}\n",
    "\\tilde{y}_0 &= z_0 \\;\\;\\; \\textrm{(which is real)}\n",
    "\\\\\n",
    "\\tilde{y}_1 + i\\tilde{y}_2 &=  z_1\n",
    "\\\\\n",
    "\\dots &= \\dots\n",
    "\\\\\n",
    "\\tilde{y}_{n-2} + i\\tilde{y}_{n-1} &= z_{n/2-1}\n",
    "\\\\\n",
    "\\tilde{y}_n &= z_{n/2} \\;\\;\\; \\textrm{(which is also real)}\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define function `sp` to perform the transformation from $f_j$ to $y_k$, multiplied by $\\frac{1}{\\sqrt{N}$, where $N$ is the number of data points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sp(y):\n",
    "    yw = spfft.rfft(y) / np.sqrt(y.shape[0])\n",
    "    # The following statement, commented out, would make the transformation matrix orthogonal.\n",
    "    #yw[1:-1] = yw[1:-1] / np.sqrt(2.0)\n",
    "    return yw"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The inverse function would be `spi`. We define it a bit more general, allowing to receive a matrix as well as a vector, since it is what is needed for the compressed sensing procedure defined below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def spi(yt):\n",
    "    if yt.ndim == 1:\n",
    "        yp = yt.copy()\n",
    "        #yp[1:-1] = yp[1:-1] / np.sqrt(2.0)\n",
    "        y = spfft.irfft(yp) * np.sqrt(yt.shape[0])\n",
    "    elif yt.ndim == 2:\n",
    "        y = yt.copy()\n",
    "        for i in range(yt.shape[1]):\n",
    "            yp = yt[:, i].copy()\n",
    "            #yp[1:-1] = yp[1:-1] / np.sqrt(2.0)\n",
    "            y[:, i] = spfft.irfft(yp) * np.sqrt(yt.shape[0])\n",
    "    return y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `realtocomplex` function gets the original complex Fourier coefficients from the real one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def realtocomplex(yw):\n",
    "    n = yw.shape[0]\n",
    "    z = np.zeros(n//2, dtype = complex)\n",
    "    z[0] = yw[0]\n",
    "    for k in range(1, n//2):\n",
    "        z[k] = complex(yw[2*k-1], yw[2*k])\n",
    "        #z[k] = np.sqrt(2.0) * complex(yw[2*k-1], yw[2*k])\n",
    "    return z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will now use the real FFT function to the same transformation that we did before with the complex one:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "yt = sp(y) * np.sqrt(dt)\n",
    "z = realtocomplex(yt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot the difference, to make sure that the two procedures are equal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(w[:n//2], np.abs(z[:n//2])-np.abs(yw[:n//2]))\n",
    "ax.set_xlim(left=0.0)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Indeed, that is the case."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let us imagine that we cannot afford to get all the data points in the fine grid. Let us try to get the Fourier transform using the coarse grid, and see how good it is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ycoarset = sp(ycoarse) * np.sqrt(dtcoarse)\n",
    "zcoarse = realtocomplex(ycoarset)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(w[:ncoarse//2], np.abs(zcoarse[:n//2]))\n",
    "ax.plot(w[:n//2], np.abs(z[:n//2]))\n",
    "ax.set_xlim(left=0.0)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is clear that it is a very wrong approximation. The reason is that the time discretization used is not enough to describe the high frequencies present in the signal."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Now, using compressed sensing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now see how the compressed sensing technique permits to obtain a good approximation, but using the same number of data points as the number used in the coarse grid."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step is to get a number of sample points from the original fine grid (a number equal to the number of data points in the coarse grid). In arrays `t2`and `y2` we store those random time points and data values of the signal, respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ri = np.random.choice(n, m, replace=False) # random sample of indices\n",
    "ri.sort() # sorting not strictly necessary, but convenient for plotting\n",
    "t2 = t[ri]\n",
    "y2 = y[ri]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot the signal in that random array of data points, to see how it looks (very ugly, but amazingly, it contains the necessary information)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(2, 1)\n",
    "ax[0].plot(t2, y2)\n",
    "ax[1].plot(t2, y2)\n",
    "ax[1].set_xlim(left = 0.0, right = 0.02)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we must build a transformation matrix `A`. First, it is defined as the transformation matrix associated to the inverse transformation in the fine grid. Then, we select only the rows associated to the selected random data points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = spi(np.identity(n))\n",
    "A = A[ri]\n",
    "#print(A.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The idea now is to minimize the convex function\n",
    "\\begin{equation}\n",
    "G(v) = \\vert\\vert v \\vert\\vert_1\n",
    "\\end{equation}\n",
    "constrained to fulfilling\n",
    "\\begin{equation}\n",
    "Av = y_2\n",
    "\\end{equation}\n",
    "\n",
    "For that task we use the cvxpy package."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v = cvx.Variable(n)\n",
    "objective = cvx.Minimize(cvx.norm(v, 1))\n",
    "constraints = [A@v == y2]\n",
    "prob = cvx.Problem(objective, constraints)\n",
    "result = prob.solve(verbose = True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reconstruct signal\n",
    "x = np.array(v.value)\n",
    "#x = np.squeeze(x)\n",
    "zx = realtocomplex(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now plot this and see how it is a good approximation to the Fourier transform of the signal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 2)\n",
    "ax[0].plot(w[:n//2], np.abs(zx[:n//2]) * np.sqrt(dt))\n",
    "ax[1].plot(w[:n//2], np.abs(z[:n//2]))\n",
    "ax[0].set_xlim(left = 0.0)\n",
    "ax[1].set_xlim(left = 0.0)\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
