import tempfile
import os
import shutil
import subprocess


def test_linear():
    tempdir = tempfile.mkdtemp()
    fname = os.path.dirname(__file__) + '/../notebooks/' + 'linear.ipynb'
    shutil.copy(fname, tempdir)
    os.chdir(tempdir)
    i = subprocess.run( 'jupyter nbconvert --to python linear.ipynb', shell = True)
    j = subprocess.run( ['python3 linear.py'], shell = True)
    assert (i.returncode, j.returncode) == (0, 0)

def test_linear_octopus():
    tempdir = tempfile.mkdtemp()
    fname = os.path.dirname(__file__) + '/../notebooks/' + 'linear-octopus.ipynb'
    shutil.copy(fname, tempdir)
    os.chdir(tempdir)
    i = subprocess.run( 'jupyter nbconvert --to python linear-octopus.ipynb', shell = True)
    j = subprocess.run( ['python3 linear-octopus.py'], shell = True)
    assert (i.returncode, j.returncode) == (0, 0)

def test_quadratic():
    tempdir = tempfile.mkdtemp()
    fname = os.path.dirname(__file__) + '/../notebooks/' + 'quadratic.ipynb'
    shutil.copy(fname, tempdir)
    os.chdir(tempdir)
    i = subprocess.run( 'jupyter nbconvert --to python quadratic.ipynb', shell = True)
    j = subprocess.run( ['python3 quadratic.py'], shell = True)
    assert (i.returncode, j.returncode) == (0, 0)

# In order to run the testsuite:
# py.test -v tests/susceptibilities_tests.py
# To get the stdout, use also the "-s" flag.


